=== NiceChat-to-WP ===
Contributors: silverice
Tags: nicechat, chat, buttonchat, live, chat, livechat
Plugin Name: NiceChat-to-WP
Version: 0.0.1
Git: https://bitbucket.org/NiceChat/nice-chat-to-wp
Author: NiceChat Team
Author URI: https://nice.chat/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
Text Domain: nice-chat-to-wp
Tested up to: 4.8
Description: The NiceChat-to-WP give you an ability to one-click integration your WordPress/WooCommerce blog with the NiceChat - the first eCommerce oriented chat.

== Description ==

The NiceChat-to-WP give you an ability to one-click integration your WordPress/WooCommerce blog with the NiceChat - the first eCommerce oriented chat.

The NiceChat is a forever free (but you can buy more featured Business and Enterprise plans) eCommerce oriented online chat solution and the fastest way to increase sales on your blog. WooCommerce is supported, but not required (you can sale with NiceChat without Internet-shop features like a cart and checkout on your site).

The first version of plugin joins NiceChat widget to your site.

Features development plans:
* export products from WooCommerce to NiceChat (after 15 Jun 2017)
* import orders and client information from NiceChat to WooCommerce (after 25 Jun 2017)
* WooCommrce cart and user integration - agents in a chat can see ID and Name of WP-logged users; when site visitor click Buy on products this product will be added to WooCommerece cart and can be purchased (Jul 2017);
* Live statistics monitoring (Sep 2017);
* and much more =)

== Installation ==

Upload the NiceChat-to-WP plugin to your blog, Activate it, register your Site on https://nice.chat/.
If you are Nice Chat partner and want to install the chat on your customer's blog register on https://partners.nice.chat/ and get the partner ID which you need to specify in the Module settings.

== Changelog ==

= 0.1.0a =
*Release Date - 03 June 2017*

* Initial release
+ Widget integrate tools