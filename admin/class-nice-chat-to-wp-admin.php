<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    NiceChat_to_WP
 * @subpackage NiceChat_to_WP/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    NiceChat_to_WP
 * @subpackage NiceChat_to_WP/admin
 * @author     SilverIce <si@nice.chat>
 */
class NiceChat_to_WP_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register page on Settings in admin-area.
	 *
	 * @since    1.0.0
	 */
	public function add_nice_chat_menu() {

		add_options_page('NiceChat_to_WP', 'NiceChat', 8, 'nice_chat_options_page', array($this, nice_chat_options_page));
	}

	public function nice_chat_options_page() {
		if($_POST['nice_chat_partner_id']) {
			// set the post formatting options
			if (preg_match("/^[a-z0-9]{20,60}$/", $_POST['nice_chat_partner_id'])) {
				$nice_chat_partner_id = $_POST['nice_chat_partner_id'];
				update_option('nice_chat_partner_id', $nice_chat_partner_id);
				echo ('<div class="updated"><p>'.__('Settings updated', 'textdomain').'</p></div>');
			} else {
				echo ('<div class="error"><p>'.__('Wrong Partner ID', 'textdomain').'</p></div>');
			}
		}
		$nice_chat_partner_id = get_option('nice_chat_partner_id');


		?>
		<div class="wrap">
			<h2><?php echo (__('Setup NiceChat Widget', 'textdomain')) ?></h2>
			<form method="post">
			<fieldset class="options">
				<p>
					<legend><?php echo (__('Partner ID (optional)', 'textdomain')) ?>:</legend>
					<input type="text" name="nice_chat_partner_id" value="<?php echo($nice_chat_partner_id)?>"/>
				</p>
				<p>
					<input type="submit" value="<?php echo (__('Send', 'textdomain')) ?>" />
				</p>
			</fieldset>
			</form>
		</div>
		<?php
	}

}
