<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    NiceChat_to_WP
 * @subpackage NiceChat_to_WP/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    NiceChat
 * @subpackage NiceChat_to_WP/includes
 * @author     SilverIce <si@nice.chat>
 */
class NiceChat_to_WP_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
